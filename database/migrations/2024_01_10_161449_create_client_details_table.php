<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('client_details', function (Blueprint $table) {
            $table->id();
            $table->string('category')->index(); // for searching
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->enum('gender', ['male', 'female'])->index(); // for searching
            $table->timestamp('birth_date')->index(); // for searching
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('client_details');
    }
};
