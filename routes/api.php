<?php

use App\Http\Controllers\ImportCsvController;
use App\Http\Controllers\IndexClientDetailsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('csv/import', ImportCsvController::class)->name('csv.import');
Route::get('client-details', IndexClientDetailsController::class)->name('client.details');
