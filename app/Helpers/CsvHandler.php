<?php

namespace App\Helpers;

use App\Data\ClientDetails;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvHandler
{
    public static function import(UploadedFile $file, int $chunk_size = 1000, string $separator = ','): bool
    {
        if ($contents = fopen($file->path(), 'r')) {
            $is_first_chunk = true;

            while (($data = fgetcsv($contents, $chunk_size, $separator)) !== false) {
                if ($is_first_chunk) {
                    $is_first_chunk = false;

                    continue;
                }

                // Store the data into the chunk array
                $chunk[] = ClientDetails::fromArray($data)->toArray();

                if (count($chunk) >= $chunk_size) {
                    // Insert the chunk into the database
                    if (\App\Models\ClientDetails::query()->insertOrIgnore($chunk)) {
                        $chunk = [];
                    }
                }
            }

            fclose($contents);

            return true;
        }

        return false;
    }

    public static function export(array $csv_data): StreamedResponse
    {
        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="client_details.csv"',
        ];

        return Response::stream(function () use ($csv_data) {
            $stream = fopen('php://output', 'w');
            fputcsv($stream, ['category', 'firstname', 'lastname', 'email', 'gender', 'birthDate']); // Add headers

            foreach ($csv_data as $row) {
                fputcsv($stream, $row);
            }

            fclose($stream);
        }, 200, $headers);
    }

    public static function getTestCsv(): UploadedFile
    {
        $file_name = 'Dataset.txt';
        $path = storage_path($file_name);

        return new UploadedFile($path, $file_name);
    }

    public static function importTestCsv(): bool
    {
        return self::import(self::getTestCsv());
    }
}
