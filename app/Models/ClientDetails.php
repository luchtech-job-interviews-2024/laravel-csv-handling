<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientDetails extends Model
{
    use HasFactory;

    protected $casts = [
        'birth_date' => 'date',
    ];

    //protected $appends = ['age'];

    public function getAgeAttribute(): int
    {
        return now()->diffInYears($this->birth_date);
    }
}
