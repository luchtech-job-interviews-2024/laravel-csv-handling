<?php

namespace App\Data;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class ClientDetails implements Arrayable
{
    public function __construct(
        public string $category,
        public string $first_name,
        public string $last_name,
        public string $email,
        public string $gender,
        public string|Carbon $birth_date,
    ) {
        $this->birth_date = Carbon::parse($birth_date);
    }

    public static function fromArray(array $data): self
    {
        if (Arr::isAssoc($data)) {
            return new self(
                $data['category'],
                $data['first_name'],
                $data['last_name'],
                $data['email'],
                $data['gender'],
                Carbon::parse($data['birth_date']),
            );
        }

        return new self(...$data);
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
