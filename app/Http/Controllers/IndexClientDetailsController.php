<?php

namespace App\Http\Controllers;

use App\Helpers\CsvHandler;
use App\Models\ClientDetails;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class IndexClientDetailsController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $should_export = $request->boolean('export');

        $data = QueryBuilder::for(ClientDetails::class)
            ->allowedFilters([
                'category',
                'first_name',
                'last_name',
                'email',
                AllowedFilter::exact('gender'),
                AllowedFilter::callback('birth_date', function (Builder $query, $value) {
                    return $query->whereDate('birth_date', '=', Carbon::parse($value)->format('Y-m-d'));
                }),
                AllowedFilter::callback('age', function (Builder $query, $value) {
                    $age = (int) $value;
                    $birth_date = now()->subYears($age);

                    return $query->whereBetween('birth_date', [$birth_date->copy()->subYear(), $birth_date]);
                }),
                AllowedFilter::callback('age_range', function (Builder $query, $value) {
                    [$min_age, $max_age] = $value;

                    $min_birth_date = now()->subYears($max_age);
                    $max_birth_date = now()->subYears($min_age);

                    return $query->whereBetween('birth_date', [$min_birth_date, $max_birth_date]);
                }),
            ]);

        if ($should_export) {
            $csv_data = [];

            foreach ($data->cursor() as $client) {
                $csv_data[] = [
                    $client->category,
                    $client->first_name,
                    $client->last_name,
                    $client->email,
                    $client->gender,
                    $client->birth_date->format('m/d/Y'),
                ];
            }

            return CsvHandler::export($csv_data);
        }

        return response()->json($data->paginate($request->get('per_page')));
    }
}
