<?php

namespace App\Http\Controllers;

use App\Helpers\CsvHandler;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ImportCsvController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $csv = $request->csv;
        $chunk_size = $request->get('chunk', 1000);
        $separator = $request->get('separator', ',');

        $success = $csv instanceof UploadedFile && CsvHandler::import($csv, $chunk_size, $separator);

        if ($success) {
            $message = 'Successfully imported data from file.';
            $code = 201;
        } else {
            $message = 'Failed to import data from file.';
            $code = 422;
        }

        return response()->json([
            'message' => $message,
        ], $code);
    }
}
