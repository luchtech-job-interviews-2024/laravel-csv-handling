<?php

use App\Data\ClientDetails;
use Illuminate\Support\Arr;

it('can convert array to ClientDetails data', function (array $data) {
    $client = ClientDetails::fromArray($data);
    expect($client)->toBeInstanceOf(ClientDetails::class);

    if (Arr::isAssoc($data)) {
        expect($client->category)->toBe($data['category'])
            ->and($client->first_name)->toBe($data['first_name'])
            ->and($client->last_name)->toBe($data['last_name'])
            ->and($client->email)->toBe($data['email'])
            ->and($client->gender)->toBe($data['gender'])
            ->and($client->birth_date->diffInSeconds($data['birth_date']))->toBe(0);
    } else {
        expect($client->category)->toBe($data[0])
            ->and($client->first_name)->toBe($data[1])
            ->and($client->last_name)->toBe($data[2])
            ->and($client->email)->toBe($data[3])
            ->and($client->gender)->toBe($data[4])
            ->and($client->birth_date->diffInSeconds($data[5]))->toBe(0);
    }
})->with([
    'normal array' => fn () => [
        'books',
        'James Carlo',
        'Luchavez',
        'jamescarloluchavez@gmail.com',
        'male',
        '09/02/1996',
    ],
    'associative array' => fn () => [
        'category' => 'books',
        'first_name' => 'James Carlo',
        'last_name' => 'Luchavez',
        'email' => 'jamescarloluchavez@gmail.com',
        'gender' => 'male',
        'birth_date' => '09/02/1996',
    ],
]);
