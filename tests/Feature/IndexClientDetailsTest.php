<?php

use App\Helpers\CsvHandler;
use App\Models\ClientDetails;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;

use function Pest\Laravel\get;

dataset('should_export', [false, true]);

beforeEach(fn () => CsvHandler::importTestCsv());

function assertCsvHasRowCount(TestResponse $response, int $expected_rows): TestResponse
{
    $path = 'test.csv';
    $disk = Storage::fake('local');
    $disk->put($path, $response->streamedContent());

    $rows = count(explode(PHP_EOL, $disk->get($path))) - 2;

    expect($rows)->toBe($expected_rows);

    return $response;
}

it('can filter by category', function (string $category, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['category' => $category],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($category) {
                assertCsvResponse($response);

                $expected = ClientDetails::query()->where(DB::raw('LOWER(category)'), 'like', '%'.strtolower($category).'%')->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['books', 'clothes'])
    ->with('should_export');

it('can filter by first name', function (string $first_name, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['first_name' => $first_name],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($first_name) {
                assertCsvResponse($response);

                $expected = ClientDetails::query()->where(DB::raw('LOWER(first_name)'), 'like', '%'.strtolower($first_name).'%')->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['John', 'Jane'])
    ->with('should_export');

it('can filter by last name', function (string $last_name, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['last_name' => $last_name],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($last_name) {
                assertCsvResponse($response);

                $expected = ClientDetails::query()->where(DB::raw('LOWER(last_name)'), 'like', '%'.strtolower($last_name).'%')->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['Jacobs', 'Davis'])
    ->with('should_export');

it('can filter by email', function (string $email, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['email' => $email],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($email) {
                assertCsvResponse($response);

                $expected = ClientDetails::query()->where(DB::raw('LOWER(email)'), 'like', '%'.strtolower($email).'%')->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['John', 'Jane'])
    ->with('should_export');

it('can filter by gender', function (string $gender, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['gender' => $gender],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($gender) {
                assertCsvResponse($response);

                $expected = ClientDetails::query()->where('gender', $gender)->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['male', 'female'])
    ->with('should_export');

it('can filter by birth date', function (string $birth_date, bool $should_export) {
    $per_page = 13;
    $params = [
        'filter' => ['birth_date' => $birth_date],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($birth_date) {
                assertCsvResponse($response);

                $date = Carbon::parse($birth_date)->format('Y-m-d');

                $expected = ClientDetails::query()->whereDate('birth_date', $date)->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => $response->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['11/22/01', 'November 22 2001'])
    ->with('should_export');

it('can filter by age', function (string $age, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['age' => $age],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($age) {
                assertCsvResponse($response);

                $birth_date = now()->subYears($age);
                $expected = ClientDetails::query()->whereBetween('birth_date', [$birth_date->copy()->subYear(), $birth_date])->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['25', '50'])
    ->with('should_export');

it('can filter by age range', function (string $age_range, bool $should_export) {
    $per_page = 15;
    $params = [
        'filter' => ['age_range' => $age_range],
        'per_page' => $per_page,
    ];

    if ($should_export) {
        $params['export'] = true;
    }

    get('/api/client-details?'.http_build_query($params))
        ->when(
            $should_export,
            function (TestResponse $response) use ($age_range) {
                assertCsvResponse($response);

                [$min_age, $max_age] = explode(',', $age_range);

                $min_birth_date = now()->subYears($max_age);
                $max_birth_date = now()->subYears($min_age);

                $expected = ClientDetails::query()->whereBetween('birth_date', [$min_birth_date, $max_birth_date])->count();
                assertCsvHasRowCount($response, $expected);
            },
            fn (TestResponse $response) => assertFastPaginated($response)->assertJsonCount($per_page, 'data'),
        )
        ->assertOk();
})
    ->with(['25,30', '30,40'])
    ->with('should_export');
