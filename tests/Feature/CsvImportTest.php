<?php

use App\Helpers\CsvHandler;
use App\Models\ClientDetails;

use function Pest\Laravel\post;

it('can import data from csv', function () {
    post('/api/csv/import', [
        'csv' => CsvHandler::getTestCsv(),
    ])
        ->assertCreated()
        ->assertJson([
            'message' => 'Successfully imported data from file.',
        ]);

    // Test if the count is 100_000
    expect(ClientDetails::query()->count())->toBe(100_000);
});
