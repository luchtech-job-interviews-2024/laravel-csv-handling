# Job Interview: CSV Handling using Laravel 10

<!-- TOC -->
* [Job Interview: CSV Handling using Laravel 10](#job-interview-csv-handling-using-laravel-10)
  * [Installation Guide](#installation-guide)
    * [Current Setup](#current-setup)
    * [Cloning the Project](#cloning-the-project)
    * [Running Laravel app using Docker](#running-laravel-app-using-docker)
    * [Setting up Laravel](#setting-up-laravel)
    * [Testing](#testing)
  * [API Endpoints](#api-endpoints)
    * [Importing CSV](#importing-csv)
    * [Filtering Client Details](#filtering-client-details)
    * [Exporting Client Details](#exporting-client-details)
<!-- TOC -->

## Installation Guide

### Current Setup

These are the pieces of software that are used to run the Laravel app:

- Git `2.41.0`
- Docker `24.0.7` (from Orbstack)
- Docker Compose `2.23.3`
- PHP `8.1.27` (inside the `app` Docker container)
- Composer `2.6.6` (inside `app` Docker container)
- MySQL `8.0.35` (inside `db` Docker container)

### Cloning the Project

If you already have Git, you can immediately clone this repository by running:
```
git clone https://gitlab.com/luchtech-job-interviews-2024/laravel-csv-handling.git csv-test
cd csv-test
```

Once cloned, we have to create a `.env` file which will be used by our Docker setup as well as our Laravel application.
To create it, we can just copy the `.env.example` file.

```
cp .env.example .env
```

Note: The database credentials inside `.env.example` are already pre-configured to work with the current Docker setup.

### Running Laravel app using Docker

As defined in the [Docker Compose file](./docker-compose.yml), we will be creating two containers: `app` and `db`.

To create them, simply run this command:
```
docker-compose up -d
```
This should download the images need before creating the Docker containers.

In your Docker Desktop or Orbstack app, you should be able to see something similar to this. This just tells us that the `app` and `db` containers are already running.

![Docker containers on Orbstack](./images/orbstack.png)

### Setting up Laravel

Once the containers are created, we should proceed to setting up our Laravel application.

In order to proceed, we should get inside the `app` container first.
This is so we can use the `php` and `composer` commands.  

To use the `app`'s terminal, run this command:
```
docker-compose exec app bash
ls 
```

The `ls` command will list the files inside the `app` container's `/var/www/html`.
And in there, you'll be seeing the Laravel files as well.
That's just means your project is mounted successfully to the container.

![Mounted Laravel files](./images/app-container-bash.png)

Below are the next commands to run:
```
composer install
php artisan key:generate
php artisan migrate:fresh
```

These commands will:
1. Install the project dependencies.
2. Generate the `APP_KEY`.
3. Run the migration files which will create the database tables.

### Testing

I've already added test files that satisfies the given requirements. Run the tests like so:
```
php artisan test
```

You should be able to see these results.

![Tests results](./images/testing.png)

## API Endpoints

In order to satisfy the filtering requirements, I opted to use [spatie/laravel-query-builder](https://spatie.be/docs/laravel-query-builder/v5/introduction).
For better pagination performance, I opted to use [hammerstone/fast-paginate](https://github.com/hammerstonedev/fast-paginate)

### Importing CSV

Endpoint: `/api/csv/import`<br/>
Method: `POST`<br/>
Headers: `Content-Type=multipart/form-data`

This is the endpoint where you can upload the CSV file.
On the body, just add `csv` key and the CSV file as value.

### Filtering Client Details

Endpoint: `/api/client-details`<br/>
Method: `GET`<br/>
Headers: `Accept=application/json`

This endpoint uses the `spatie/laravel-query-builder` package so please refer to their documentation for more details.

Here are the available filters:

| Filter     | Type    | Example                        |
|------------|---------|--------------------------------|
| category   | partial | `?filter[category]=books`      |
| first_name | partial | `?filter[first_name]=John`     |
| last_name  | partial | `?filter[last_name]=Davis`     |
| email      | partial | `?filter[email]=john`          |
| gender     | exact   | `?filter[gender]=Davis`        |
| birth_date | exact   | `?filter[birth_date]=11/22/01` |
| age        | exact   | `?filter[age]=25`              |
| age_range  | exact   | `?filter[age_range]=25,30`     |

### Exporting Client Details

Endpoint: `/api/client-details?export=1`<br/>
Method: `GET`<br/>

This endpoint is just the same with the endpoint [above](#filtering-client-details) except this one has an additional param called `export`.
The `?export=1` just indicates that instead of returning a JSON response, it will return a streamed CSV response instead.

Since this is the same endpoint as above, the same filters will also work.
